## Usage

Run commands from the root directory.

To search for a regex in in `path/to/file`:

    from siregex import grep
    search_results = grep(r'<regular expression>', "path/to/file")

In this example, `search_results` now has a dictionary where the keys are line numbers that contain a match, and the values are a list of pairs representing matches within that line, where the first value is the start of a match and the last value is the end of a match. (Lines count from 1 and  columns count from 0, start and ending indexs are inclusive; column count should be incremented by 1 when visualizing.)

For example, in the text

      0        10        20        30       40
      |         |         |         |        |
    1 Four score and seven years ago our fathers
    2 brought forth on this continent a new nation,
    3 conceived in liberty, and dedicated to the 
    4 proposition that all men are created equal.

The command

    grep(r'and', "/path/to/file")

Should return:

    {
        1: [(11, 13)],
        3: [(22, 24)],
    }

## Tests

To run the tests, go to the root of the project and run:

    python -m unittest discover tests


import unittest

from siregex.models import parse_regex_string, TokenList, CharToken


class TestParseRegex(unittest.TestCase):

    def test_parse_chars(self):
        rstring = "abc"
        regex = parse_regex_string(rstring)
        for i, token in enumerate(regex.tokens):
            self.assertEqual(token.char, rstring[i])
            self.assertTrue(isinstance(token, CharToken))

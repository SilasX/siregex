import unittest
from siregex.models import CharToken


class TestToken(unittest.TestCase):

    def test_single_char(self):
        line = "abcdcba"
        c_token = CharToken("c")
        expected1 = [(2, 2), (4, 4)]
        actual1 = c_token.find_matches(line)
        self.assertEqual(expected1, actual1)

import os.path as op
import unittest

from siregex.models import parse_regex_string, grep

CUR_DIR = op.dirname(op.abspath(__file__))
FILE_DIR = op.join(CUR_DIR, "files")


class TestParseRegex(unittest.TestCase):

    def test_parse_chars_two(self):
        # Just fine one result for now
        expected = [(0, 2), (26, 28)]
        line = "and conceived in liberty, and dedicated to the"
        regex = parse_regex_string("and")
        actual = regex.find_matches(line)
        self.assertEqual(expected, actual)

    def test_parse_or(self):
        pass
        # # Just find one result for now
        # expected = [(0, 2), (5, 6), (26, 28),]
        # line = "and conceived in liberty, and dedicated to the"
        # regex = parse_regex_string("[ao]")
        # actual = regex.find_matches(line)
        # self.assertEqual(expected, actual)

    def test_parse_file(self):
        expected = {
            1: [(11, 13)],
            3: [(22, 24)],
        }
        regex = "and"
        getts = op.join(FILE_DIR, "gettysburg.txt")
        actual = grep(regex, getts)
        self.assertEqual(expected, actual)

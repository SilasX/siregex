# General approach:
# There are two general phases: parsing the regex string into an internal
# representation, and then searching through text for matches based on
# that representation. A utility function parse_regex_string will convert
# the string into the tokenzied version, and the tokenized version will
# read through a string to find the matches.

# Representing the regex:
# A regex is composed of a list of "tokens". Each token represents a list
# of possible matches. For example, the regex `[acq]` represents a token
# that is matched by the characters a, c, or q. The program must first
# parse a string into tokens, then search through the lines of a file,
# checking whether it can match a subset to the first token, then the
# second, and so on.  Since each token is an OR of possible regexes, the
# structure must support recursively "or" ing other regexes together.

# Thus, there is a TokenList class to support ordering of tokens (used
# as the core regex class), and a TokenOr class to support "or"ing
# together a set of possible tokens

# Each token class must support a .find_matches(str) method for
# returning a (start_index, end_index) pair of all matches.
# It must also support a .matches_from (start_index, str) method that
# finds all token matches in str that *start* at start_index, and
# returns a list their final characters


class NoMatch(Exception):
    """Exception class for short-circuting when it knows there's
    no way to match"""
    pass


class TokenList(object):
    """Root regex object; contains the list of tokens to be matched in
    order
    """

    def __init__(self, tokens=None):
        # Previous and next tokens
        if tokens is None:
            self.tokens = []
        else:
            self.tokens = tokens

    def add_token(self, token):
        self.tokens.append(token)

    def find_matches(self, line):
        # find a match for first token, then recursively
        # branch off each token to find the matches that extend it
        results = []
        initial_matches = self.tokens[0].find_matches(line)
        for match_start, match_end in initial_matches:
            # Search for extensions starting at 1 + final index of
            # each match, trying to match the second token
            try:
                match_exts = self.matches_from(match_end + 1, 1, line)
                for match_ext_start, match_ext_end in match_exts:
                    results.append((match_start, match_ext_end))
            except NoMatch:
                continue
        return results

    def matches_from(self, start_index, token_index, line):
        """return all the matches that start at start_index in line
        as tuple of (start, end) indices"""
        # throw NoMatch exception if there are tokens remaining
        # but past end of string
        if token_index < len(self.tokens) and start_index >= len(line):
            raise NoMatch
        # if all tokens matched, index still in string (or one more)
        # then return a match starting an ending in previous string
        # so it can be correctly merged
        if token_index >= len(self.tokens) and start_index <= len(line):
            return [(start_index - 1, start_index - 1)]
        results = []
        # Otherwise, still matches to be made and still chars left in
        # string; throw NoMatch exception if current token not matched

        # First, match current token
        matches = self.tokens[token_index].matches_from(
            start_index, line)
        if len(matches) == 0:
            raise NoMatch
        for matchi in matches:
            # each result is a final index, so search for the next token
            # the index after that
            match_exts = self.matches_from(
                matchi + 1, token_index + 1, line)
            for match_ext_start, match_ext_end in match_exts:
                results.append((start_index, match_ext_end))
        return results


class TokenOr(object):
    """Class for matching any one of several token classes
    """
    def __init__(self, token_ops):
        self.token_ops = token_ops  # List of allowable tokens


class CharToken(object):
    """class for matching single character
    """

    def __init__(self, char):
        self.char = char

    def find_matches(self, line):
        """return a list of (initial, final) index pairs that
        match the token, starting from `start"""
        results = []
        for line_i in xrange(len(line)):
            if line[line_i] == self.char:
                results.append((line_i, line_i))
        return results

    def matches_from(self, start_index, line):
        """Return a list of the final indices of all blocks of
        characters that match the token and start at `start_index`"""
        if line[start_index] == self.char:
            return [start_index]
        else:
            return []


def parse_regex_string(rst):
    """Given a regex string, return a regex object (TokenList) to
    represent it"""
    # Basic version, assume it's all simple characters
    regex = TokenList()
    for ch in rst:
        regex.add_token(CharToken(ch))
    return regex


def grep(regex_str, filepath):
    result = {}
    regex = parse_regex_string(regex_str)
    line_no = 1
    with open(filepath, "r") as f:
        for line in f:
            fmt_line = line.rstrip('\n\r')
            match_list = regex.find_matches(fmt_line)
            if len(match_list) > 0:
                result[line_no] = match_list
            line_no += 1
    return result
